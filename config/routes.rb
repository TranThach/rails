Rails.application.routes.draw do
  get 'password_resets/new'
  get 'password_resets/edit'
  get 'sessions/new'
  get 'static_pages/home'
  get 'static_pages/help'

  get  'static_pages/home'
  get  'static_pages/help', as: 'help_page'
  get  'static_pages/contact'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :microposts
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]

  # root 'application#hello'
  root "static_pages#home"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get "/register", to: "user#register"
  post "/register", to: "user#register_post"
  mount Sidekiq::Web => "/sidekiq" # monitoring console

  namespace :admin do
    resources :post, :user
  end

end
